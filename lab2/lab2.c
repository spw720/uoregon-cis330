#include <stdio.h>
#include <stdbool.h>

bool arrayEqual(int a[2][4], int b[2][4], int m, int n){
	for (int i = 0; i<m; i++){
		for (int j = 0; j<n; j++){
			if (a[i][j] != b[i][j]) return false;
		}
	}
	return true;
}


int main() {

	int a[2][4] = {{1,2,3,4},{5,6,7,8}};
	int b[2][4] = {{1,2,3,4},{5,6,7,8}};


	bool result = arrayEqual(a,b,2,4);

	if (result == true) printf("TRUE\n");
	if (result == false) printf("FALSE\n");

}
