#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "clock.h"



// Initialize the clock data structure 
void initClock( ClockType *clock ){
	int hours, minutes, seconds;

        //int hour1, hour10, minute1, minute10, second1, second10;

	time_t now = clock->time;
	//clock->time = now;

        time(&now);//gets current time from time_t constant

        struct tm *local = localtime(&now);//struct to allow parsing of hour/minute/second

        hours = local->tm_hour;//(0-23)
        minutes = local->tm_min;//(0-59)
        seconds = local->tm_sec;//(0-59)

        //converting the 2-digit integer H/M/S-times to 1 digit numbers to-be translated into ASCII
        
        if (seconds < 9){
                clock->second1 = seconds;
                clock->second10 = 0;
        }
        else {
                clock->second1 = (seconds - (seconds / 10) * 10);
                clock->second10 = (seconds / 10);
        }

        if (minutes < 9){
                clock->minute1 = minutes;
                clock->minute10 = 0;
        }
        else {
                clock->minute1 = (minutes - (minutes / 10) * 10);
                clock->minute10 = (minutes / 10);
        }
        if (hours < 9){
                clock->hour1 = hours;
                clock->hour10 = 0;
        }
        else {
                clock->hour1 = (hours - (hours / 10) * 10);
                clock->hour10 = (hours / 10);
        }

}//end of initClock




// Print an ASCII clock showing cur_time as the time 
void printClock( const time_t cur_time, const ClockType *clock ){

        //#1
        char* array_1[] = {"  1 ",
                           " 11 ",
                           "  1 ",
                           "  1 ",
                           "  1 ",
                           "  1 ",
                           "  1 ",
                           " 111"};
	//#2
        char* array_2[] = {" 22 ",
                           "2  2",
                           "   2",
                           "   2",
                           "  2 ",
                           " 2  ",
                           "2   ",
                           "2222"};
        //#3
        char* array_3[] = {" 33 ",
                           "3  3",
                           "   3",
                           "  3 ",
                           " 333",
                           "   3",
                           "3  3",
                           " 33 "};
        //#4
        char* array_4[] = {"4 4 ",
                           "4 4 ",
                           "4 4 ",
                           "4444",
                           "  4 ",
                           "  4 ",
                           "  4 ",
                           "  4 "};
	//#5
        char* array_5[] = {"5555",
                           "5   ",
                           "5   ",
                           "5 55",
                           "   5",
                           "5  5",
                           "5  5",
                           " 55 "};
        //#6
        char* array_6[] = {" 66 ",
                           "6  6",
                           "6   ",
                           "6   ",
                           "666 ",
                           "6  6",
                           "6  6",
                           " 66 "};
        //#7
        char* array_7[] = {"7777",
                           "   7",
                           "   7",
                           "  7 ",
                           " 7  ",
                           " 7  ",
                           " 7  ",
                           " 7  "};
        //#8
        char* array_8[] = {" 88 ",
                           "8  8",
                           "8  8",
                           " 88 ",
                           "8  8",
                           "8  8",
                           "8  8",
                           " 88 "};
	//#9
        char* array_9[] = {" 99 ",
                           "9  9",
                           "9  9",
                           " 999",
                           "   9",
                           "   9",
                           "   9",
                           "   9"};
        //#0
        char* array_0[] = {" 00 ",
                           "0  0",
                           "0 00",
                           "0 00",
                           "00 0",
                           "00 0",
                           "0  0",
                           " 00 "};
        //# :
        //char **array_col = malloc(8 * sizeof(char *));
        //for (int i = 0; i < 8; ++i) {
        //        array_col[i] = malloc(4 * sizeof(char));
        //}
        char* array_col[] =    {"    ",
                                "    ",
                                "  : ",
                                "  : ",
                                "    ",
                                "  : ",
                                "  : ",
                                "    "};	

	//messy start to for loop for printing each line of ASCII arrays
	for (int i = 0; i < 8; i++){
	
		switch (clock->hour10){
			case 1 : printf("%s", array_1[i]);//H10 = array_1;
				 break;
			case 2 : printf("%s", array_2[i]);
				 break;
			case 3 : printf("%s", array_3[i]);
				 break;
			case 4 : printf("%s", array_4[i]);
				 break;
			case 5 : printf("%s", array_5[i]);
				 break;
			case 6 : printf("%s", array_6[i]);
				 break;
			case 7 : printf("%s", array_7[i]);
				 break;
			case 8 : printf("%s", array_8[i]);
				 break;
			case 9 : printf("%s", array_9[i]);
				 break;
			case 0 : printf("%s", array_0[i]);
				 break;
		}
		printf("  ");
		switch (clock->hour1){
			case 1 : printf("%s", array_1[i]);
				 break;
                        case 2 : printf("%s", array_2[i]);
				 break;
                        case 3 : printf("%s", array_3[i]);
				 break;
                        case 4 : printf("%s", array_4[i]);
				 break;
                        case 5 : printf("%s", array_5[i]);
				 break;
                        case 6 : printf("%s", array_6[i]);
				 break;
                        case 7 : printf("%s", array_7[i]);
				 break;
                        case 8 : printf("%s", array_8[i]);
				 break;
                        case 9 : printf("%s", array_9[i]);
				 break;
                        case 0 : printf("%s", array_0[i]);
				 break;
                }
		printf("  ");
		printf("%s", array_col[i]);
		printf("  ");
		switch (clock->minute10){
			case 1 : printf("%s", array_1[i]);
				 break;
                        case 2 : printf("%s", array_2[i]);
				 break;
                        case 3 : printf("%s", array_3[i]);
				 break;
                        case 4 : printf("%s", array_4[i]);
				 break;
                        case 5 : printf("%s", array_5[i]);
				 break;
                        case 6 : printf("%s", array_6[i]);
				 break;
                        case 7 : printf("%s", array_7[i]);
				 break;
                        case 8 : printf("%s", array_8[i]);
				 break;
                        case 9 : printf("%s", array_9[i]);
				 break;
                        case 0 : printf("%s", array_0[i]);
				 break;
                }
		printf("  ");
		switch (clock->minute1){
                	case 1 : printf("%s", array_1[i]);
				 break;
                        case 2 : printf("%s", array_2[i]);
				 break;
                        case 3 : printf("%s", array_3[i]);
				 break;
                        case 4 : printf("%s", array_4[i]);
				 break;
                        case 5 : printf("%s", array_5[i]);
				 break;
                        case 6 : printf("%s", array_6[i]);
				 break;
                        case 7 : printf("%s", array_7[i]);
				 break;
                        case 8 : printf("%s", array_8[i]);
				 break;
                        case 9 : printf("%s", array_9[i]);
				 break;
                        case 0 : printf("%s", array_0[i]);
				 break;
		}
		printf("  ");
		printf("%s", array_col[i]);
		printf("  ");
		switch (clock->second10){
                	case 1 : printf("%s", array_1[i]);
				 break;
                        case 2 : printf("%s", array_2[i]);
				 break;
                        case 3 : printf("%s", array_3[i]);
				 break;
                        case 4 : printf("%s", array_4[i]);
				 break;
                        case 5 : printf("%s", array_5[i]);
				 break;
                        case 6 : printf("%s", array_6[i]);
				 break;
                        case 7 : printf("%s", array_7[i]);
				 break;
                        case 8 : printf("%s", array_8[i]);
				 break;
                        case 9 : printf("%s", array_9[i]);
				 break;
                        case 0 : printf("%s", array_0[i]);
				 break;
		}
		printf("  ");
		switch (clock->second1){
                	case 1 : printf("%s", array_1[i]);
				 break;
                        case 2 : printf("%s", array_2[i]);
				 break;
                        case 3 : printf("%s", array_3[i]);
				 break;
                        case 4 : printf("%s", array_4[i]);
				 break;
                        case 5 : printf("%s", array_5[i]);
				 break;
                        case 6 : printf("%s", array_6[i]);
				 break;
                        case 7 : printf("%s", array_7[i]);
				 break;
                        case 8 : printf("%s", array_8[i]);
				 break;
                        case 9 : printf("%s", array_9[i]);
				 break;
                        case 0 : printf("%s", array_0[i]);
				 break;
		}
		printf("\n");
	}//end of for loop

}//end of printClock()



// Free up any dynamically allocated memory in the clock
void cleanClock( ClockType *clock){
	free(clock);
}//end of cleanClock()

