#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "timer.h"

int main(){

        ClockType clock;

        ClockType *pt_clock;

        pt_clock = &clock;

    	unsigned int minute, second;
    	char str[5];
	
	printf("How long should the timer run (MM:SS)? ");
	scanf("%s", str);

    	if(sscanf(str, "%u:%u", &minute, &second) == 2)
        	printf("You entered minute=%u, second=%u\n", minute, second);
	else {
		printf("ERROR: Incorrect Input\n");
		exit(1);
	}//end of else

	initTimer(pt_clock, minute, second);
	runTimer();	

        return 0;

}//end of main()
