
#ifndef CLOCK_H_
#define CLOCK_H_

#include <stdlib.h>

typedef struct {
	// TODO: Define your ASCII clock data structure here. 

	const time_t time;

	int second1;
	int second10;
	
	int minute1;
	int minute10;

	int hour1;
	int hour10;
	
} ClockType;//end of ClockType struct

// Initialize the clock data structure 
void initClock( ClockType *clock );

// Print an ASCII clock showing cur_time as the time 
void printClock( const time_t cur_time, const ClockType *clock );//TODO Change back to project parameters

// Free up any dynamically allocated memory in the clock
void cleanClock( ClockType *clock);

#endif /* CLOCK_H_ */
