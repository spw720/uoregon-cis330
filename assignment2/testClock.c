#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "clock.h"

int main(){

	ClockType clock;

	ClockType *pt_clock;

	pt_clock = &clock;

	initClock(pt_clock);

	printClock(pt_clock->time, pt_clock);
	
	return 0;

}//end of main()
