typedef struct {
        // TODO: Define your ASCII clock data structure here. 

        int minute1;
	int minute10;

        int second1;
	int second10;

} ClockType;//end of ClockType struct


// Initialize the timer with the user-provided input
void initTimer(ClockType *clock, int minutes, int seconds);

// Run the timer -- print out the time each second
void runTimer();

// Clean up memory (as needed)
void cleanTimer(ClockType  *clock);
