#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "timer.h"


int numMinutes = 0;
int numSeconds = 0;

// Initialize the timer with the user-provided input
void initTimer(ClockType *clock, int minutes, int seconds){
	
	numMinutes = minutes;
	numSeconds = seconds;


}//end of initTimer method

// Run the timer -- print out the time each second
void runTimer(){

	int M1, M10, S1, S10;

	//#1
        char* array_1[] = {"  1 ",
                           " 11 ",
                           "  1 ",
                           "  1 ",
                           "  1 ",
                           "  1 ",
                           "  1 ",
                           " 111"};
        //#2
        char* array_2[] = {" 22 ",
                           "2  2",
                           "   2",
                           "   2",
                           "  2 ",
                           " 2  ",
                           "2   ",
                           "2222"};
        //#3
        char* array_3[] = {" 33 ",
                           "3  3",
                           "   3",
                           "  3 ",
                           " 333",
                           "   3",
                           "3  3",
                           " 33 "};
        //#4
        char* array_4[] = {"4 4 ",
                           "4 4 ",
                           "4 4 ",
                           "4444",
                           "  4 ",
                           "  4 ",
                           "  4 ",
                           "  4 "};
        //#5
        char* array_5[] = {"5555",
                           "5   ",
                           "5   ",
                           "5 55",
                           "   5",
                           "5  5",
                           "5  5",
                           " 55 "};
        //#6
        char* array_6[] = {" 66 ",
                           "6  6",
                           "6   ",
                           "6   ",
                           "666 ",
                           "6  6",
                           "6  6",
                           " 66 "};
	//#7
        char* array_7[] = {"7777",
                           "   7",
                           "   7",
                           "  7 ",
                           " 7  ",
                           " 7  ",
                           " 7  ",
                           " 7  "};
        //#8
        char* array_8[] = {" 88 ",
                           "8  8",
                           "8  8",
                           " 88 ",
                           "8  8",
                           "8  8",
                           "8  8",
                           " 88 "};
        //#9
        char* array_9[] = {" 99 ",
                           "9  9",
                           "9  9",
                           " 999",
                           "   9",
                           "   9",
                           "   9",
                           "   9"};
        //#0
        char* array_0[] = {" 00 ",
                           "0  0",
                           "0 00",
                           "0 00",
                           "00 0",
                           "00 0",
                           "0  0",
                           " 00 "};
	//#:
        char* array_col[] =    {"    ",
                                "    ",
                                "  : ",
                                "  : ",
                                "    ",
                                "  : ",
                                "  : ",
                                "    "};
	
	
	for (int j = numSeconds; j >= 0; j--){

		if (j < 9){
                	S1 = j;
                	S10 = 0;
        	}
       		else {
               		S1 = (j - (j / 10) * 10);
               		S10 = (j / 10);
       		}

		if (numMinutes < 9){
                       	M1 = numMinutes;
                       	M10 = 0;
               	}
               	else {
                       	M1 = (numMinutes - (numMinutes / 10) * 10);
                       	M10 = (numMinutes / 10);
               	}

		for (int xi = 0; xi < 8; xi++){
			switch (M10){
                        	case 1 : printf("%s", array_1[xi]);
                                	 break;
                        	case 2 : printf("%s", array_2[xi]);
                        	         break;
                        	case 3 : printf("%s", array_3[xi]);
                        	         break;
                        	case 4 : printf("%s", array_4[xi]);
                        	         break;
                        	case 5 : printf("%s", array_5[xi]);
                        	         break;
                        	case 6 : printf("%s", array_6[xi]);
                        	         break;
                        	case 7 : printf("%s", array_7[xi]);
                        	         break;
                        	case 8 : printf("%s", array_8[xi]);
                        	         break;
                        	case 9 : printf("%s", array_9[xi]);
                        	         break;
                        	case 0 : printf("%s", array_0[xi]);
                        	         break;
                	}
			printf("   ");
			switch (M1){
                        	case 1 : printf("%s", array_1[xi]);
                        	         break;
                        	case 2 : printf("%s", array_2[xi]);
                        	         break;
                        	case 3 : printf("%s", array_3[xi]);
                        	         break;
                        	case 4 : printf("%s", array_4[xi]);
                        	         break;
                        	case 5 : printf("%s", array_5[xi]);
                        	         break;
                        	case 6 : printf("%s", array_6[xi]);
                        	         break;
                        	case 7 : printf("%s", array_7[xi]);
                        	         break;
                        	case 8 : printf("%s", array_8[xi]);
                        	         break;
                        	case 9 : printf("%s", array_9[xi]);
                        	         break;
                        	case 0 : printf("%s", array_0[xi]);
                        	         break;
                	}
			printf(" ");
			printf("%s", array_col[xi]);
			printf("   ");
			switch (S10){
                        	case 1 : printf("%s", array_1[xi]);
                               	       	break;
                        	case 2 : printf("%s", array_2[xi]);
                                	 break;
                        	case 3 : printf("%s", array_3[xi]);
                                 	break;
                        	case 4 : printf("%s", array_4[xi]);
                                	 break;
                        	case 5 : printf("%s", array_5[xi]);
                                	 break;
                        	case 6 : printf("%s", array_6[xi]);
                                	 break;
                        	case 7 : printf("%s", array_7[xi]);
                        	         break;
                       		case 8 : printf("%s", array_8[xi]);
                        	         break;
                        	case 9 : printf("%s", array_9[xi]);
                        	         break;
                        	case 0 : printf("%s", array_0[xi]);
                        	         break;
                	}
			printf("   ");
			switch (S1){
                        	case 1 : printf("%s", array_1[xi]);
                        	         break;
                        	case 2 : printf("%s", array_2[xi]);
                        	         break;
                        	case 3 : printf("%s", array_3[xi]);
                        	         break;
                        	case 4 : printf("%s", array_4[xi]);
                        	         break;
                        	case 5 : printf("%s", array_5[xi]);
                        	         break;
                        	case 6 : printf("%s", array_6[xi]);
                        	         break;
                        	case 7 : printf("%s", array_7[xi]);
                        	         break;
                        	case 8 : printf("%s", array_8[xi]);
                        	         break;
                        	case 9 : printf("%s", array_9[xi]);
                        	         break;
                        	case 0 : printf("%s", array_0[xi]);
                        	         break;
                	}

			printf("\n");

		}//end of 8 for loop

		printf("\n");   
                sleep(1);

        }//end od nsecond for loop

	numMinutes -= 1;
	while(numMinutes>=0){


		for (int i = 59; i >= 0; i--){

			if (i < 9){
                       		S1 = i;
                       		S10 = 0;
               		}
               		else {
                       		S1 = (i - (i / 10) * 10);
                       		S10 = (i / 10);
               		}
                	if (numMinutes < 9){
                       		M1 = numMinutes;
                       		M10 = 0;
               		}
               		else {
                       		M1 = (numMinutes - (numMinutes / 10) * 10);
                       		M10 = (numMinutes / 10);
               		}

                		
			for (int xy = 0; xy < 8; xy++){
				switch (M10){
                        		case 1 : printf("%s", array_1[xy]);
                        		         break;
                        		case 2 : printf("%s", array_2[xy]);
                        		         break;
                        		case 3 : printf("%s", array_3[xy]);
                        		         break;
                        		case 4 : printf("%s", array_4[xy]);
                        		         break;
                        		case 5 : printf("%s", array_5[xy]);
                        		         break;
                        		case 6 : printf("%s", array_6[xy]);
                        		         break;
                        		case 7 : printf("%s", array_7[xy]);
                        		         break;
                        		case 8 : printf("%s", array_8[xy]);
                        		         break;
                        		case 9 : printf("%s", array_9[xy]);
                        		         break;
                        		case 0 : printf("%s", array_0[xy]);
                        	       		 break;
                		}
				printf("   ");
                		switch (M1){
                		        case 1 : printf("%s", array_1[xy]);
                		                 break;
                		        case 2 : printf("%s", array_2[xy]);
                		                 break;
                		        case 3 : printf("%s", array_3[xy]);
                		                 break;
                		        case 4 : printf("%s", array_4[xy]);
                		                 break;
                		        case 5 : printf("%s", array_5[xy]);
                		                 break;
                		        case 6 : printf("%s", array_6[xy]);
                		                 break;
                		        case 7 : printf("%s", array_7[xy]);
                		                 break;
                		        case 8 : printf("%s", array_8[xy]);
                		                 break;
                		        case 9 : printf("%s", array_9[xy]);
                		                 break;
                		        case 0 : printf("%s", array_0[xy]);
                		                 break;
                		}
				printf(" ");
				printf("%s", array_col[xy]);
				printf("   ");
                		switch (S10){
                		        case 1 : printf("%s", array_1[xy]);
                		                 break;
                		        case 2 : printf("%s", array_2[xy]);
                		                 break;
                		        case 3 : printf("%s", array_3[xy]);
                		                 break;
                		        case 4 : printf("%s", array_4[xy]);
                		                 break;
                		        case 5 : printf("%s", array_5[xy]);
                		                 break;
                		        case 6 : printf("%s", array_6[xy]);
                        		         break;
                        		case 7 : printf("%s", array_7[xy]);
                        		         break;
                        		case 8 : printf("%s", array_8[xy]);
                        		         break;
                        		case 9 : printf("%s", array_9[xy]);
                        		         break;
                        		case 0 : printf("%s", array_0[xy]);
                        	        	 break;
                		}
				printf("   ");
                		switch (S1){
                		        case 1 : printf("%s", array_1[xy]);
                		                 break;
                		        case 2 : printf("%s", array_2[xy]);
                		                 break;
                		        case 3 : printf("%s", array_3[xy]);
                		                 break;
                		        case 4 : printf("%s", array_4[xy]);
                		                 break;
                		        case 5 : printf("%s", array_5[xy]);
                		                 break;
                		        case 6 : printf("%s", array_6[xy]);
                		                 break;
                		        case 7 : printf("%s", array_7[xy]);
                		                 break;
                		        case 8 : printf("%s", array_8[xy]);
                		                 break;
                		        case 9 : printf("%s", array_9[xy]);
                		                 break;
                		        case 0 : printf("%s", array_0[xy]);
                		                 break;
                		}

				printf("\n");

			}//end of 8 for loop

			printf("\n");			
			sleep(1);

		}

		numMinutes -= 1;

	}//end of while loop


}//end of runTimer method

// Clean up memory (as needed)
void cleanTimer(ClockType  *clock);
