#include "cipher.hpp"
#include "caesar.hpp"
#include <iostream>
using namespace std;


CaesarCipher::CaesarCipher() : Cipher() {
        // Nothing else to do in the constructor
}

std::string CaesarCipher::encrypt( std::string &inputText ){
	std::string text = inputText;	
	std::string::size_type len = text.length();
        //int key;
        //cout << "Enter encrypt key: ";
        //cin >> key;
	int key = 3;

	for (int i = 0; i != len; ++i) {
		text[i] = text[i] + key;
	}

	//cout << text << endl;	

	return text;

}//end of encrypt()


std::string CaesarCipher::decrypt( std::string &inputText ){
	std::string text = inputText;
	std::string::size_type len = text.length();
        //int key;
        //cout << "Enter decrypt key: ";
        //cin >> key;
	int key = 3;

        for (int i = 0; i != len; ++i) {
                text[i] = text[i] - key;
        }

	//cout << text << endl;

        return text;

}//end of decrypt()



