//Date
//

#ifndef DATE_HPP_
#define DATE_HPP_


#include <string>
#include "cipher.hpp"


class DateCipher : public Cipher{

        public:

                DateCipher();

                virtual std::string encrypt( std::string &text );
                virtual std::string decrypt( std::string &text );

};

#endif

