#include <iostream>
#include <fstream>

#include "ioutils.hpp"
#include "cipher.hpp"
#include "caesar.hpp"

//using namespace std;

/*
int main(){
	
        //CaesarCipher *cz = new CaesarCipher();
        CaesarCipher cz;

        std::string H;

        cout << "Enter string to be encrypted: ";
        getline(cin, H);
        //cin >> H;

        std::string *Hp = &H;

        cz.encrypt(*Hp);

        cz.decrypt(*Hp);


}
*/

int main(int argc, const char *argv[]) {

        IOUtils io;
        io.openStream(argc,argv);
        std::string input, encrypted, decrypted;
        input = io.readFromStream();
        std::cout << "\nOriginal text:" << std::endl;
       	std::cout << input << std::endl;

	CaesarCipher cz;
        
	encrypted = cz.encrypt(input);

  	std::cout << "\nEncrypted text:" << std::endl; //<< std::endl << encrypted;  
	std::cout << encrypted << std::endl;

	decrypted = cz.decrypt(encrypted);
        
	std::cout << "\nDecrypted text:" << std::endl;
	std::cout << decrypted << std::endl; 

        if (decrypted == input) std::cout << "Decrypted text matches input!" << std::endl;
        
	else {
                std::cout << "Oops! Decrypted text doesn't match input!" << std::endl;
                return 1;   // Make sure to return a non-zero value to indicate failure
        }

        return 0;
}

