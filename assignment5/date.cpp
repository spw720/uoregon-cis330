#include "cipher.hpp"
#include "date.hpp"
#include <iostream>
using namespace std;

DateCipher::DateCipher() : Cipher() {
        // Nothing else to do in the constructor
}

std::string DateCipher::encrypt( std::string &inputText ){

	std::string text = inputText;
        std::string::size_type len = text.length();
       
	int DD = 1, D = 1, MM = 1, M = 1, YYYY = 1, YYY = 1, YY = 1, Y = 1;
	
	int iter = 0;

	for (int i = 0; i != len; i++) {
                if (text[i] == ' ') continue;
		if (iter == 0) {
			text[i] = text[i] + DD;
			iter += 1;
		}
		if (iter == 1) {
                        text[i] = text[i] + D;
                        iter += 1;
                }
		if (iter == 2) {
                        text[i] = text[i] + MM;
                        iter += 1;
                }
		if (iter == 3) {
                        text[i] = text[i] + M;
                        iter += 1;
                }
		if (iter == 4) {
                        text[i] = text[i] + YYYY;
                        iter += 1;
                }
		if (iter == 5) {
                        text[i] = text[i] + YYY;
                        iter += 1;
                }
		if (iter == 6) {
                        text[i] = text[i] + YY;
                        iter += 1;
                }
		if (iter == 7) {
                        text[i] = text[i] + Y;
                        iter = 0;
                }
	}
        //cout << text << endl;

        return text;

}//end of encrypt()


/*
std::string text = inputText;
        std::string::size_type len = text.length();
        for (int i = 0; i != len; ++i) {
        if (text[i] >= 'a' && text[i] <= 'm') {
            text[i] = text[i] + 13;
        } else if (text[i] >= 'n' && text[i] <= 'z') {
            text[i] = text[i] - 13;
        } else if(text[i] >= 'A' && text[i] <= 'M') {
            text[i] = text[i] + 13;
        } else if(text[i] >= 'N' && text[i] <= 'Z') {
            text[i] = text[i] - 13;
        }
        }
        return text;
}
*/


std::string DateCipher::decrypt( std::string &inputText ){
	std::string text = inputText;
	std::string::size_type len = text.length();

        int DD = 1, D = 1, MM = 1, M = 1, YYYY = 1, YYY = 1, YY = 1, Y = 1;

        int iter = 0;

        for (int i = 0; i != len; i++) {
                if (text[i] == ' ') continue;
                if (iter == 0) {
                        text[i] = text[i] - DD;
                        iter += 1;
                }
                if (iter == 1) {
                        text[i] = text[i] - D;
                        iter += 1;
                }
                if (iter == 2) {
                        text[i] = text[i] - MM;
                        iter += 1;
                }
                if (iter == 3) {
                        text[i] = text[i] - M;
                        iter += 1;
                }
                if (iter == 4) {
                        text[i] = text[i] - YYYY;
                        iter += 1;
                }
                if (iter == 5) {
                        text[i] = text[i] - YYY;
                        iter += 1;
                }
                if (iter == 6) {
                        text[i] = text[i] - YY;
                        iter += 1;
                }
                if (iter == 7) {
                        text[i] = text[i] - Y;
                        iter = 0;
                }
        }

        return text;

}//end of decrypt()
