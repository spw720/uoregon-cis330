#include <stdio.h>
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_RED     "\x1b[31m"
#include "reversi.h"


void initBoard( GameBoard  *board ){

	int center, dimension, i, j, x;

        printf("\nPlease enter the size of the board: ");
        scanf("%d", &dimension);

	board->size = dimension;//send dimensions to struct

	char **array = malloc(dimension * sizeof(char *));//allocate row pointers
	
	for(i = 0; i < dimension; i++)
  		array[i] = malloc(dimension * sizeof(char));//allocate each row separately	
	
	for (j = 0; j < dimension; j++){
		for(x = 0; x < dimension; x++)
			array[j][x] = '.';
	}//end of (j) for loop

	center = dimension/2;//calculate center of the board

	array[center-1][center-1] = '0';
	array[center-1][center] = 'X';
	array[center][center-1] = 'X';
	array[center][center] = '0';

	board->p1_score = 0;//initialize p1 score to 0
	board->p2_score = 0;//initialize p2 score to 0

	board->g_board = array;//send board to struct

	printBoard(board);

}//end of initBoard()

//**********************************************************************************************************************************************

void printBoard( GameBoard *board ){
	
	int i, j, z, dimension;

	dimension = board->size;//get dimension from struct
	
	checkScore(board);

	printf("\n   ");

	for (z = 0; z < board->size; z++) {//for loop to print column numbers at top of board	
		if (z < 9) printf("%d  ", z+1);
		else printf("%d ", z+1);
	}//end of (z) for loop

	printf("\n");

	for (i = 0; i < board->size; i++){

		printf("%d ", i+1);//printing row nubers on side

		if (i < 9) printf(" ");//create space to allign board

                for (j = 0; j < board->size; j++) {
                	if (board->g_board[i][j] == '0') printf(/*ANSI_COLOR_GREEN*/ "%c  "/* ANSI_COLOR_RESET TODO*/, board->g_board[i][j]);
			if (board->g_board[i][j] == 'X') printf(/* TODO ANSI_COLOR_YELLOW*/ "%c  " /*ANSI_COLOR_RESET*/, board->g_board[i][j]);
			if (board->g_board[i][j] == '.') printf("%c  " , board->g_board[i][j]);
		}

		printf("\n\n");

	}//end of (i) for loop

	printf("\n");

}//end of printBoard()

//**********************************************************************************************************************************************

int checkPerim(GameBoard *board, int x, int y){//checker to make sure a coordinate has at least one other block in perimeter
	
	x -= 1;
	y -= 1;

	int max = board->size;
	max -= 1;

	if(x==0 && y==0){//x=0, y=0 (top-left)
		if(
			board->g_board[x+1][y] != '.' ||  //S
			board->g_board[x+1][y+1] != '.' || //SE
			board->g_board[x][y+1] != '.'     //E	
		  )
			return 1;
		else return 0;
	}
	
	if(x==max && y==max){//x=max, y=max (bottom-right)
                if(
                        board->g_board[x-1][y-1] != '.' ||//NW
                        board->g_board[x][y-1] != '.' ||  //W
                        board->g_board[x-1][y] != '.'     //N
                  )
                        return 1;
                else return 0;
        }

	if(x==max && y==0){//x=max, y=0 (bottom-left)
                if(
                        board->g_board[x-1][y] != '.' ||  //N
                        board->g_board[x-1][y+1] != '.' ||//NE
                        board->g_board[x][y+1] != '.'     //E
                  )
                        return 1;
                else return 0;
        }
	
	if(x==0 && y==max){//x=0, y=max (top-right)
                if(
                        board->g_board[x][y-1] != '.' ||  //W
                        board->g_board[x+1][y-1] != '.' ||//SW
                        board->g_board[x+1][y] != '.'     //S
                  )
                        return 1;
                else return 0;
        }

	if(y==0){//x=0 (left)
        	if(
                        board->g_board[x-1][y] != '.' ||  //N
                        board->g_board[x+1][y] != '.' ||  //S
                        board->g_board[x-1][y+1] != '.' ||//NE
                        board->g_board[x][y+1] != '.' ||  //E
                        board->g_board[x+1][y+1] != '.'  //SE
                  )
                        return 1;
                else return 0;
	}

	if(x==0){//y=0 (top)
		if(
                        board->g_board[x][y-1] != '.' ||  //W
                        board->g_board[x+1][y-1] != '.' ||//SW
                        board->g_board[x+1][y] != '.' ||  //S
                        board->g_board[x][y+1] != '.' ||  //E
                        board->g_board[x+1][y+1] != '.'  //SE
                  )
                        return 1;
                else return 0;
	}

	if(y==max){//x=max (right)
		if(
			board->g_board[x-1][y-1] != '.' ||//NW
                        board->g_board[x][y-1] != '.' ||  //W
                        board->g_board[x+1][y-1] != '.' ||//SW
                        board->g_board[x-1][y] != '.' ||  //N
                        board->g_board[x+1][y] != '.'     //S
                  )
                        return 1;
                else return 0;
	}

	if(x==max){//y=max (bottom)
		if(
			board->g_board[x-1][y-1] != '.' ||//NW
                        board->g_board[x][y-1] != '.' ||  //W
                        board->g_board[x-1][y] != '.' ||  //N
                        board->g_board[x-1][y+1] != '.' ||//NE
                        board->g_board[x][y+1] != '.'     //E
                  )
                        return 1;
                else return 0;
        }

	else {
		if ( 	board->g_board[x-1][y-1] != '.' ||//NW
             		board->g_board[x][y-1] != '.' ||  //W
             		board->g_board[x+1][y-1] != '.' ||//SW
             		board->g_board[x-1][y] != '.' ||  //N
             		board->g_board[x+1][y] != '.' ||  //S
             		board->g_board[x-1][y+1] != '.' ||//NE
             		board->g_board[x][y+1] != '.' ||  //E
             		board->g_board[x+1][y+1] != '.' ) //SE
			return 1;
		else return 0;
	}

}//end of checkPerim()

//**********************************************************************************************************************************************

char playerInverse(char player) {
	if (player == 'X') return '0';
	if (player == '0') return 'X';
}//end of playerInverse()

//**********************************************************************************************************************************************

void checkFlank(GameBoard *board, char player, int _row, int _col){
	
	int length, N, S, E, W, NW, NE, SW, SE;

	int i, j, x, y;

	_row -= 1;
	_col -= 1;

	length = (board->size - 1);

	// EAST -> WEST
	int CC1 = 0;//flag for invalid sequence
	for (i = _col; i >= 0; i--)
		if (board->g_board[_row][i] == player) {	
			
			for (int ix = _col-2; ix >= i; ix--) //check that spaces between hit and point are of opposite player values 
                                if (board->g_board[_row][ix+1] != playerInverse(player)) CC1 = 1;//set flag if a mismatch found

			for (int ix2 = _col-1; ix2 >= i; ix2--)  //if flag was never set, fill in values
				if (CC1 == 0) board->g_board[_row][ix2] = player;
			
			i=-1;

		}//end of if statement
	
	// WEST -> EAST
	int CC2 = 0;//flag for invalid sequence
	for (j = _col; j <= length; j++)//TODO changed to <=
                if (board->g_board[_row][j] == player) {


			for (int ix = _col+2; ix <= j; ix++) //check that spaces between hit and point are of opposite player values 
                                if (board->g_board[_row][ix-1] != playerInverse(player)) CC2 = 1;//set flag if a mismatch found

			for (int ix2 = _col+1; ix2 <= j; ix2++)  //if flag was never set, fill in values
				if (CC2 == 0) board->g_board[_row][ix2] = player;
			
			j=length;
		
		}//end of if statement

	// SOUTH -> NORTH
	int CC3 = 0;//flag for invalid sequence
	for (x = _row; x >= 0; x--)
                if (board->g_board[x][_col] == player) {


			for (int ix = _row-2; ix >= x; ix--) //check that spaces between hit and point are of opposite player values 
                                if (board->g_board[ix+1][_col] != playerInverse(player)) CC3 = 1;//set flag if a mismatch found

			for (int ix2 = _row-1; ix2 >= x; ix2--)  //if flag was never set, fill in values
				if (CC3 == 0) board->g_board[ix2][_col] = player;
			
			x=-1;

		}//end of if statement

	// NORTH -> SOUTH
	int CC4 = 0;//flag for invalid sequence
        for (y = _row; y <= length; y++)//TODO changed to <=

                if (board->g_board[y][_col] == player) {


			for (int ix = _row+2; ix <= y; ix++) { //check that spaces between hit and point are of opposite player values 
				if (board->g_board[ix-1][_col] != playerInverse(player)) CC4 = 1;//set flag if a mismatch found
			}

			for (int ix2 = _row+1; ix2 <= y; ix2++) { //if flag was never set, fill in values
                                if (CC4 == 0) board->g_board[ix2][_col] = player;
                        }

			y = length;//break out of for loop

		}//end of if statement


	//################ START OF DIAGONAL CHECKS ################
	
	// -> SOUTH-EAST
        int DC1 = 0;

	for (int temp = 1; temp <= length; temp++){
                if (_row == length || _col == length) break;// if move is in south/west/or southwest corner break
	
		if (board->g_board[_row + 1][_col + 1] == player) break; //TODO unsure about this implementation

		if (board->g_board[_row + temp][_col + temp] == player) {

			for (int temp2 = 1; temp2 < temp; temp2++){ // check all spaces up to match found
                		if (board->g_board[_row + temp2][_col + temp2] != playerInverse(player)) DC1 = 1;//if a space is found that is not playerInverse, break
        		}

			for (int temp3 = 1; temp3 <= temp; temp3++){ //change all spaces in between if correct sequence was found
				if (DC1 == 0) board->g_board[_row + temp3][_col + temp3] = player;
			}

		}//end of if

		if (_row+temp == length || _col+temp == length) break;//if move plus iteration puts iterator on edge , break

	}//end of for(temp)


	// -> SOUTH-WEST
        int DC2 = 0;

        for (int temp = 1; temp <= length; temp++){
                if (_row == length || _col == 0) break;// if move is in south/west/or southwest corner break
	
		if (board->g_board[_row + 1][_col - 1] == player) break;

                if (board->g_board[_row + temp][_col - temp] == player) { //match is found

                        for (int temp2 = 1; temp2 < temp; temp2++){ // check all spaces up to match found
                                if (board->g_board[_row + temp2][_col - temp2] != playerInverse(player)) DC2 = 1;//if a space is found that is not playerInverse, break
                        }

                        for (int temp3 = 1; temp3 <= temp; temp3++){ //change all spaces in between if correct sequence was found
                                if (DC2 == 0) board->g_board[_row + temp3][_col - temp3] = player;
                        }

                }//end of if

                if (_row+temp == length || _col-temp == 0) break;//if move plus iteration puts iterator on edge , break

        }//end of for(temp)


	// -> NORTH-EAST
        int DC3 = 0;

        for (int temp = 1; temp <= length; temp++){
                if (_row == 0 || _col == length) break;// if move is in south/west/or southwest corner break

		if (board->g_board[_row - 1][_col + 1] == player) break;

                if (board->g_board[_row - temp][_col + temp] == player) {

                        for (int temp2 = 1; temp2 < temp; temp2++){ // check all spaces up to match found
                                if (board->g_board[_row - temp2][_col + temp2] != playerInverse(player)) DC3 = 1;//if a space is found that is not playerInverse, break
                        }

                        for (int temp3 = 1; temp3 <= temp; temp3++){ //change all spaces in between if correct sequence was found
                                if (DC3 == 0) board->g_board[_row - temp3][_col + temp3] = player;
                        }

                }//end of if

                if (_row-temp == 0 || _col+temp == length) break;//if move plus iteration puts iterator on edge , break

        }//end of for(temp)


	// -> NORTH-WEST
        int DC4 = 0;

        for (int temp = 1; temp <= length; temp++){
                if (_row == 0 || _col == 0) break;// if move is in south/west/or southwest corner break

		if (board->g_board[_row - 1][_col - 1] == player) break;

                if (board->g_board[_row - temp][_col - temp] == player) {

                        for (int temp2 = 1; temp2 < temp; temp2++){ // check all spaces up to match found
                                if (board->g_board[_row - temp2][_col - temp2] != playerInverse(player)) DC4 = 1;//if a space is found that is not playerInverse, break
                        }

                        for (int temp3 = 1; temp3 <= temp; temp3++){ //change all spaces in between if correct sequence was found
                                if (DC4 == 0) board->g_board[_row - temp3][_col - temp3] = player;
                        }

                }//end of if

                if (_row-temp == 0 || _col-temp == 0) break;//if move plus iteration puts iterator on edge , break

        }//end of for(temp)	
	

}//end of checkFlank()

//**********************************************************************************************************************************************

void playGame(GameBoard *board){
	
	int moveRow, moveCol;

        while (moveRow != 99 && moveCol != 99){

                moveRow = 0;
                moveCol = 0;

		p1_redo :
                printf( /*ANSI_COLOR_YELLOW TODO*/ "\nPLAYER 1 (X) : please enter your move (row,col) (or 99,99 to exit program): "/* ANSI_COLOR_RESET*/);
                scanf("%d,%d", &moveRow, &moveCol);

                if (moveRow == 99 && moveCol == 99) {
			printf(/* TODO  ANSI_COLOR_RED*/ "\n*** Program Terminated (Game Over) ***\n\n"/* ANSI_COLOR_RESET*/);
			break;
		}	
		
		checkFlank(board, 'X', moveRow, moveCol);

		if (checkPerim(board, moveRow, moveCol) == 1 && board->g_board[moveRow-1][moveCol-1] == '.') board->g_board[moveRow-1][moveCol-1] = 'X';//check that place is not occupied
		else {
			printf("\n!!!INVALID MOVE!!!\n");
			goto p1_redo;
		}

                printBoard(board);


		moveRow = 0;
                moveCol = 0;

		p2_redo :
                printf(/* TODO  ANSI_COLOR_GREEN*/ "\nPLAYER 2 (0) : please enter your move (row,col) (or 99,99 to exit program): "/*ANSI_COLOR_RESET*/);
                scanf("%d,%d", &moveRow, &moveCol);

                if (moveRow == 99 && moveCol == 99) {
			printf(/* TODO  ANSI_COLOR_RED*/ "\n*** Program Terminated (Game Over) ***\n\n"/* ANSI_COLOR_RESET*/ );
			break;
		}

		checkFlank(board, '0', moveRow, moveCol);

		if (checkPerim(board, moveRow, moveCol) == 1 && board->g_board[moveRow-1][moveCol-1] == '.') board->g_board[moveRow-1][moveCol-1] = '0';//check that place is not occupied
		else {
			printf("\n!!!INVALID MOVE!!!\n");
			goto p2_redo;
		}

                printBoard(board);

        }//end of while loop

}//end of playGame()

//**********************************************************************************************************************************************

void checkScore(GameBoard *board){//calculates the number of each 'color' on the board

	int x, j, dimension, temp1_score, temp2_score;
	
	dimension = board->size;

	temp1_score = 0;
	temp2_score = 0;

	for (j = 0; j < dimension; j++){
                for(x = 0; x < dimension; x++){
                        if (board->g_board[j][x] == 'X') {
				temp1_score += 1;
			}
			if (board->g_board[j][x] == '0') {
				temp2_score += 1;
			}
                }//end of (x) for loop
	}//end of (j) for loop

	board->p1_score = temp1_score;//send current scores to board
	board->p2_score = temp2_score;

	printf(/* TODO  ANSI_COLOR_YELLOW*/ "\nPlayer 1 score : %d"/* ANSI_COLOR_RESET ANSI_COLOR_GREEN*/ "\tPlayer 2 score : %d\n"/* ANSI_COLOR_RESET*/ , board->p1_score, board->p2_score);

}//end of checkScore()

//**********************************************************************************************************************************************

void cleanBoard( GameBoard *board ){
	
	int i;

	for (i = 0; i < board->size; i++)//free contents of array
		free((void *)board->g_board[i]);

	free((void *)board->g_board);//free array

}//end of cleanBoard()



