#include <stdlib.h>

typedef enum {yellow, green} playerColor;

typedef struct {

	//enum playerColor p1color = yellow;
	//enum playerColor p2color = green;	

	int size;

	int p1_score;
	int p2_score;

	char **g_board;


} GameBoard;//end of GameBoard struct

void initBoard();

void printBoard(GameBoard *board);

void playGame(GameBoard *board);

void checkScore(GameBoard *board);

void cleanBoard(GameBoard *board);
