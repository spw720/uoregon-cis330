#include "reversi.h"

int main(){

        GameBoard Gboard;

        GameBoard *pt_board;

        pt_board = &Gboard;

        initBoard(pt_board);

        //printBoard(pt_board);

	playGame(pt_board);

        cleanBoard(pt_board);

        return 0;
}
