#include "reversi.hpp"
#include <iostream>
using namespace std;  

int main(){

        GameBoard Gboard;

        //GameBoard *pt_board;

        //pt_board = &Gboard;

        Gboard.initBoard();

        Gboard.playGame();

        Gboard.cleanBoard();

        return 0;
} 
