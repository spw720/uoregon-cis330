#include "reversi.hpp"
#include <iostream>
using namespace std;


void GameBoard::initBoard(void){

        int center, dimension, i, j, x;

        //printf("\nPlease enter the size of the board: ");
        //scanf("%d", &dimension);
	
	cout << "\nPlease enter the size of the board: ";
	cin >> dimension;

        size = dimension;//send dimensions to struct

        //char **array = new(dimension * sizeof(char *));//allocate row pointers
	
	char **array = nullptr;
	array = new char * [dimension];


        for(i = 0; i < dimension; i++){
                //array[i] = new(dimension * sizeof(char));//allocate each row separately 
		array[i] = new char[dimension];
	}

        for (j = 0; j < dimension; j++){
                for(x = 0; x < dimension; x++)
                        array[j][x] = '.';
        }//end of (j) for loop

        center = dimension/2;//calculate center of the board

        array[center-1][center-1] = '0';
        array[center-1][center] = 'X';
        array[center][center-1] = 'X';
        array[center][center] = '0';

        p1_score = 0;//initialize p1 score to 0
        p2_score = 0;//initialize p2 score to 0

        g_board = array;//send board to struct

        printBoard();

}//end of initBoard()

//**********************************************************************************************************************************************

void GameBoard::printBoard(void){

        int i, j, z, dimension;

        dimension = size;//get dimension from struct

        checkScore();

        //printf("\n   ");
	cout << "\n   ";


        for (z = 0; z < size; z++) {//for loop to print column numbers at top of board   
                if (z < 9) cout << z+1 << "  ";//printf("%d  ", z+1);
                else cout << z+1 << " ";//printf("%d ", z+1);
        }//end of (z) for loop

        cout << "\n";//printf("\n");

        for (i = 0; i < size; i++){

                cout << i+1 << " ";//printf("%d ", i+1);//printing row nubers on side

                if (i < 9) cout << " ";//printf(" ");//create space to allign board

                for (j = 0; j < size; j++) {
                        if (g_board[i][j] == '0') cout << g_board[i][j] << "  ";//printf("%c  "  , g_board[i][j]);
                        if (g_board[i][j] == 'X') cout << g_board[i][j] << "  ";//printf("%c  " , g_board[i][j]);
                        if (g_board[i][j] == '.') cout << g_board[i][j] << "  ";//printf("%c  " , g_board[i][j]);
                }

                cout << "\n\n";//printf("\n\n");

        }//end of (i) for loop

        cout << "\n";//printf("\n");

}//end of printBoard()

//**********************************************************************************************************************************************

int GameBoard::checkPerim(int x, int y){//checker to make sure a coordinate has at least one other block in perimeter

        x -= 1;
        y -= 1;

        int max = size;
        max -= 1;

        if(x==0 && y==0){//x=0, y=0 (top-left)
                if(
                        g_board[x+1][y] != '.' ||  //S
                        g_board[x+1][y+1] != '.' || //SE
                        g_board[x][y+1] != '.'     //E
                  )
                        return 1;
                else return 0;
        }

        if(x==max && y==max){//x=max, y=max (bottom-right)
                if(
                        g_board[x-1][y-1] != '.' ||//NW
                        g_board[x][y-1] != '.' ||  //W
                        g_board[x-1][y] != '.'     //N
                  )
                        return 1;
                else return 0;
        }

        if(x==max && y==0){//x=max, y=0 (bottom-left)
                if(
                        g_board[x-1][y] != '.' ||  //N
                        g_board[x-1][y+1] != '.' ||//NE
                        g_board[x][y+1] != '.'     //E
                  )
                        return 1;
                else return 0;
        }

        if(x==0 && y==max){//x=0, y=max (top-right)
                if(
                        g_board[x][y-1] != '.' ||  //W
                        g_board[x+1][y-1] != '.' ||//SW
                        g_board[x+1][y] != '.'     //S
                  )
                        return 1;
                else return 0;
        }

        if(y==0){//x=0 (left)
                if(
                        g_board[x-1][y] != '.' ||  //N
                        g_board[x+1][y] != '.' ||  //S
                        g_board[x-1][y+1] != '.' ||//NE
                        g_board[x][y+1] != '.' ||  //E
                        g_board[x+1][y+1] != '.'  //SE
                  )
                        return 1;
                else return 0;
        }
	if(x==0){//y=0 (top)
                if(
                        g_board[x][y-1] != '.' ||  //W
                        g_board[x+1][y-1] != '.' ||//SW
                        g_board[x+1][y] != '.' ||  //S
                        g_board[x][y+1] != '.' ||  //E
                        g_board[x+1][y+1] != '.'  //SE
                  )
                        return 1;
                else return 0;
        }

        if(y==max){//x=max (right)
                if(
                        g_board[x-1][y-1] != '.' ||//NW
                        g_board[x][y-1] != '.' ||  //W
                        g_board[x+1][y-1] != '.' ||//SW
                        g_board[x-1][y] != '.' ||  //N
                        g_board[x+1][y] != '.'     //S
                  )
                        return 1;
                else return 0;
        }

        if(x==max){//y=max (bottom)
                if(
                        g_board[x-1][y-1] != '.' ||//NW
                        g_board[x][y-1] != '.' ||  //W
                        g_board[x-1][y] != '.' ||  //N
                        g_board[x-1][y+1] != '.' ||//NE
                        g_board[x][y+1] != '.'     //E
                  )
                        return 1;
                else return 0;
        }

        else {
                if (    g_board[x-1][y-1] != '.' ||//NW
                        g_board[x][y-1] != '.' ||  //W
                        g_board[x+1][y-1] != '.' ||//SW
                        g_board[x-1][y] != '.' ||  //N
                        g_board[x+1][y] != '.' ||  //S
                        g_board[x-1][y+1] != '.' ||//NE
                        g_board[x][y+1] != '.' ||  //E
                        g_board[x+1][y+1] != '.' ) //SE
                        return 1;
                else return 0;
        }

}//end of checkPerim()

//**********************************************************************************************************************************************

char GameBoard::playerInverse(char player) {
        if (player == 'X') return '0';
        if (player == '0') return 'X';
	else return '.';
}//end of playerInverse()

//**********************************************************************************************************************************************

void GameBoard::checkFlank(char player, int _row, int _col){

        int length, N, S, E, W, NW, NE, SW, SE;

        int i, j, x, y;

        _row -= 1;
        _col -= 1;

        length = (size - 1);

        // EAST -> WEST
        int CC1 = 0;//flag for invalid sequence
        for (i = _col; i >= 0; i--)
                if (g_board[_row][i] == player) {

                        for (int ix = _col-2; ix >= i; ix--) //check that spaces between hit and point are of opposite player values
                                if (g_board[_row][ix+1] != playerInverse(player)) CC1 = 1;//set flag if a mismatch found

                        for (int ix2 = _col-1; ix2 >= i; ix2--)  //if flag was never set, fill in values
                                if (CC1 == 0) {
                                        hasMove = 1;//TODO
                                        g_board[_row][ix2] = player;
                                }
                        i=-1;

                }//end of if statement

        // WEST -> EAST
        int CC2 = 0;//flag for invalid sequence
        for (j = _col; j <= length; j++)
                if (g_board[_row][j] == player) {


                        for (int ix = _col+2; ix <= j; ix++) //check that spaces between hit and point are of opposite player values
                                if (g_board[_row][ix-1] != playerInverse(player)) CC2 = 1;//set flag if a mismatch found

                        for (int ix2 = _col+1; ix2 <= j; ix2++)  //if flag was never set, fill in values
                                if (CC2 == 0) {
                                        hasMove = 1;//TODO
                                        g_board[_row][ix2] = player;
                                }
                        j=length;

                }//end of if statement

	// SOUTH -> NORTH
        int CC3 = 0;//flag for invalid sequence
        for (x = _row; x >= 0; x--)
                if (g_board[x][_col] == player) {


                        for (int ix = _row-2; ix >= x; ix--) //check that spaces between hit and point are of opposite player values 
                                if (g_board[ix+1][_col] != playerInverse(player)) CC3 = 1;//set flag if a mismatch found

                        for (int ix2 = _row-1; ix2 >= x; ix2--)  //if flag was never set, fill in values
                                if (CC3 == 0) {
                                        hasMove = 1;//TODO
                                        g_board[ix2][_col] = player;
                                }
                        x=-1;

                }//end of if statement

        // NORTH -> SOUTH
        int CC4 = 0;//flag for invalid sequence
        for (y = _row; y <= length; y++)

                if (g_board[y][_col] == player) {


                        for (int ix = _row+2; ix <= y; ix++) { //check that spaces between hit and point are of opposite player values 
                                if (g_board[ix-1][_col] != playerInverse(player)) CC4 = 1;//set flag if a mismatch found
                        }

                        for (int ix2 = _row+1; ix2 <= y; ix2++) { //if flag was never set, fill in values
                                if (CC4 == 0) {
                                        hasMove = 1;//TODO
                                        g_board[ix2][_col] = player;
                                }
                        }

                        y = length;//break out of for loop

                }//end of if statement


        //################ START OF DIAGONAL CHECKS ################
	//
	// -> SOUTH-EAST
        int DC1 = 0;

        for (int temp = 1; temp <= length; temp++){
                if (_row == length || _col == length) break;// if move is in south/west/or southwest corner break

                if (g_board[_row + 1][_col + 1] == player) break;

                if (g_board[_row + temp][_col + temp] == player) {

                        for (int temp2 = 1; temp2 < temp; temp2++){ // check all spaces up to match found
                                if (g_board[_row + temp2][_col + temp2] != playerInverse(player)) DC1 = 1;//if a space is found that is not playerInverse, break
                        }

                        for (int temp3 = 1; temp3 <= temp; temp3++){ //change all spaces in between if correct sequence was found
                                if (DC1 == 0) {
                                        hasMove = 1;//TODO
                                        g_board[_row + temp3][_col + temp3] = player;
                                }
                        }

                }//end of if

                if (_row+temp == length || _col+temp == length) break;//if move plus iteration puts iterator on edge , break

        }//end of for(temp)


        // -> SOUTH-WEST
        int DC2 = 0;

        for (int temp = 1; temp <= length; temp++){
                if (_row == length || _col == 0) break;// if move is in south/west/or southwest corner break

                if (g_board[_row + 1][_col - 1] == player) break;

                if (g_board[_row + temp][_col - temp] == player) { //match is found

                        for (int temp2 = 1; temp2 < temp; temp2++){ // check all spaces up to match found
                                if (g_board[_row + temp2][_col - temp2] != playerInverse(player)) DC2 = 1;//if a space is found that is not playerInverse, break
                        }

                        for (int temp3 = 1; temp3 <= temp; temp3++){ //change all spaces in between if correct sequence was found
                                if (DC2 == 0) {
                                        hasMove = 1;//TODO
                                        g_board[_row + temp3][_col - temp3] = player;
                                }
                        }

                }//end of if

                if (_row+temp == length || _col-temp == 0) break;//if move plus iteration puts iterator on edge , break

        }//end of for(temp)

	// -> NORTH-EAST
        int DC3 = 0;

        for (int temp = 1; temp <= length; temp++){
                if (_row == 0 || _col == length) break;// if move is in south/west/or southwest corner break

                if (g_board[_row - 1][_col + 1] == player) break;

                if (g_board[_row - temp][_col + temp] == player) {

                        for (int temp2 = 1; temp2 < temp; temp2++){ // check all spaces up to match found
                                if (g_board[_row - temp2][_col + temp2] != playerInverse(player)) DC3 = 1;//if a space is found that is not playerInverse, break
                        }

                        for (int temp3 = 1; temp3 <= temp; temp3++){ //change all spaces in between if correct sequence was found
                                if (DC3 == 0) {
                                        hasMove = 1;//TODO
                                        g_board[_row - temp3][_col + temp3] = player;
                                }
                        }

                }//end of if

                if (_row-temp == 0 || _col+temp == length) break;//if move plus iteration puts iterator on edge , break

        }//end of for(temp)


        // -> NORTH-WEST
        int DC4 = 0;

        for (int temp = 1; temp <= length; temp++){
                if (_row == 0 || _col == 0) break;// if move is in south/west/or southwest corner break

                if (g_board[_row - 1][_col - 1] == player) break;

                if (g_board[_row - temp][_col - temp] == player) {

                        for (int temp2 = 1; temp2 < temp; temp2++){ // check all spaces up to match found
                                if (g_board[_row - temp2][_col - temp2] != playerInverse(player)) DC4 = 1;//if a space is found that is not playerInverse, break
                        }

                        for (int temp3 = 1; temp3 <= temp; temp3++){ //change all spaces in between if correct sequence was found
                                if (DC4 == 0) {
                                        hasMove = 1;//TODO
                                        g_board[_row - temp3][_col - temp3] = player;
                                }
                        }

                }//end of if

                if (_row-temp == 0 || _col-temp == 0) break;//if move plus iteration puts iterator on edge , break

        }//end of for(temp)


}//end of checkFlank()

//**********************************************************************************************************************************************
//
void GameBoard::playGame(void){

        int moveRow, moveCol, end_cond;

        while (moveRow != 99 && moveCol != 99){

		end_cond = 1;

                for (int j2 = 0; j2 < size; j2++){ //for loop to check if board is full
                        for(int x2 = 0; x2 < size; x2++){
                                if (g_board[j2][x2] == '.') {
                                        end_cond = 0;
                                }

                        }
                }

                if (end_cond == 1) {
			checkScore();
                        if (p1_score < p2_score) cout << "\nPlayer 2 (0) Wins!";
                        if (p2_score < p1_score) cout << "\nPlayer 1 (X) Wins!";
                        if (p2_score == p1_score) cout << "\nTIE!";
                        cout << "\n\n***GAME OVER***\n\n";
			break;
                }

                p1_redo :
		
		moveRow = 0;
                moveCol = 0;

                cout << "\nPLAYER 1 (X) : please enter your move (row,col) (or 99,99 to exit program): ";
		cout << "\n(or -- enter 90,90 to skip your turn if none available) ";
		cin >> moveRow;
    		cin.get();//TODO cin.ignore(numeric_limits<streamsize>::max(), ',');
    		cin >> moveCol;


                if (moveRow == 99 && moveCol == 99) {
                        cout << "\n*** Program Terminated (Game Over) ***\n\n";
			break;
                }
		if (moveRow == 90 && moveCol == 90) {
                        goto p2_redo;
                }

                checkFlank('X', moveRow, moveCol);

                if (hasMove == 0){//TODO
                        cout << "\nPLAYER 1 : NOT A VALID MOVE\n";
			goto p1_redo;
                }

                if (checkPerim(moveRow, moveCol) == 1 && g_board[moveRow-1][moveCol-1] == '.') g_board[moveRow-1][moveCol-1] = 'X';//check that place is not occupied
                else {
                        cout << "\n!!!INVALID MOVE!!!\n";
			goto p1_redo;
                }

                hasMove = 0;//TODO

                printBoard();

		end_cond = 1;//reset game over condition

		for (int j2 = 0; j2 < size; j2++){ //for loop to check if board is full
                        for(int x2 = 0; x2 < size; x2++){
                                if (g_board[j2][x2] == '.') {
                                        end_cond = 0;
                                }

                        }
                }

                if (end_cond == 1) {
                        checkScore();
                        if (p1_score < p2_score) cout << "\nPlayer 2 (0) Wins!";
                        if (p2_score < p1_score) cout << "\nPlayer 1 (X) Wins!";
                        if (p2_score == p1_score) cout << "\nTIE!";
			cout << "\n\n***GAME OVER***\n\n";
                        break;
                }

		p2_redo :

                moveRow = 0;
                moveCol = 0;

		cout << "\nPLAYER 2 (0) : please enter your move (row,col) (or 99,99 to exit program): ";
		cout << "\n(or -- enter 90,90 to skip your turn if none available) ";
		cin >> moveRow;
                cin.get();//TODO cin.ignore(numeric_limits<streamsize>::max(), ',');
                cin >> moveCol;


                if (moveRow == 99 && moveCol == 99) {
                        cout << "\n*** Program Terminated (Game Over) ***\n\n";
			break;
                }
		if (moveRow == 90 && moveCol == 90) {
                        goto p1_redo;
                }

                checkFlank('0', moveRow, moveCol);

                if (hasMove == 0){//TODO
                        cout << "\nPLAYER 2 : NOT A VALID MOVE\n";
			goto p2_redo;
                }

                if (checkPerim(moveRow, moveCol) == 1 && g_board[moveRow-1][moveCol-1] == '.') g_board[moveRow-1][moveCol-1] = '0';//check that place is not occupied
                else {
                        cout << "\n!!!INVALID MOVE!!!\n";
			goto p2_redo;
                }

                hasMove = 0;//TODO

                printBoard();




        }//end of while loop

}//end of playGame()

//**********************************************************************************************************************************************

void GameBoard::checkScore(void){//calculates the number of each 'color' on the board

        int x, j, dimension, temp1_score, temp2_score;

        dimension = size;

        temp1_score = 0;
        temp2_score = 0;

        for (j = 0; j < dimension; j++){
                for(x = 0; x < dimension; x++){
                        if (g_board[j][x] == 'X') {
                                temp1_score += 1;
                        }
                        if (g_board[j][x] == '0') {
                                temp2_score += 1;
                        }
                }//end of (x) for loop
        }//end of (j) for loop

        p1_score = temp1_score;//send current scores to board
        p2_score = temp2_score;

        //printf( "\nPlayer 1 score : %d\tPlayer 2 score : %d\n" , p1_score, p2_score);
	cout << "\nPlayer 1 score : " << p1_score << "\tPlayer 2 score : " << p2_score << endl;


}//end of checkScore()

//**********************************************************************************************************************************************

void GameBoard::cleanBoard(void){

        int i;

        for (i = 0; i < size; i++) {//free contents of array        
		//delete((void *)board->g_board[i]);
		delete []  g_board[i];
	}
        //delete((void *)board->g_board);//free array
	delete [] g_board;

}//end of cleanBoard()






