#include <iostream>
using namespace std;

/*
typedef struct {

        int size;

        int hasMove;

        int p1_score;
        int p2_score;

        char **g_board;


} GameBoard;//end of GameBoard struct
*/

class GameBoard {
	public:
		int size;

        	int hasMove;

        	int p1_score;
        	int p2_score;

        	char **g_board;

		// Function Declarations *************************

		void initBoard(void);

		void printBoard(void);

		int checkPerim(int x, int y);

		char playerInverse(char player);

		void checkFlank(char player, int _row, int _col);

		void playGame(void);

		void checkScore(void);

		void cleanBoard(void);

		
};

//void initBoard(GameBoard *board);

//void printBoard(GameBoard *board);

//void playGame(GameBoard *board);

//void checkScore(GameBoard *board);

//void cleanBoard(GameBoard *board);

