#include <math.h>
#include <stdio.h>
#include "power.h"

double power(double a, double b){
	
	double final = a;
	for (int i = 2; i <= b; i++)
		final = final*a;
	
	return final;
}

int main(){
	printf("%f\n", power(2, 3));
}
